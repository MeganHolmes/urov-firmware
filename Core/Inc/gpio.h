/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    gpio.h
  * @brief   This file contains all the function prototypes for
  *          the gpio.c file
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GPIO_H__
#define __GPIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */
/* Macros Defines ----------------------------------------------------------- */
#define CHECK_RETURN_VAL(rv, trv) if (trv != HAL_OK) \
		rv = trv

typedef enum
{
	RUDDER_DRIVE_TOGETHER = 0,
	RUDDER_DRIVE_DIFF = 1
} Rudder_Drive_Direction;

typedef enum
{
	Bouyancy_Direction_HOLD = 0,
	Bouyancy_Direction_UP = 1,
	Bouyancy_Direction_DOWN = 2
} Bouyancy_Direction;

struct commands
{
	uint16_t thruster;
	uint16_t elevator;
	uint16_t rudder;
	Rudder_Drive_Direction rudder_drive;
	Bouyancy_Direction bouyancy_control;
	Bouyancy_Direction last_bouyancy_control;
};
/* USER CODE END Private defines */

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */
void blinky(void);
void error_blink(void);
void green_blinky(void);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ GPIO_H__ */

