/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    tim.c
  * @brief   This file provides code for the configuration
  *          of the TIM instances.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "tim.h"

/* USER CODE BEGIN 0 */
#include <stdlib.h>

#include "sensors.h"

#define MAX_16BIT 65535
#define ESC_INIT_DUTY 2000
#define MIN_PWM_INIT_DELAY_IN_MS 3000
#define MAX_TANK_PERCENT 99.0f
#define TANK_PERCENT_PER_S 7.142
#define TIMER_TO_S 1/4000
#define SERVO_0_DUTY 4782

static void pump_down();
static HAL_StatusTypeDef rudder_control(struct commands* cmds);
static HAL_StatusTypeDef buoyancy_control(struct commands* cmds,
		struct sensor_data* data_ptr);
static HAL_StatusTypeDef restart_buoyancy_timer();
static void update_tank_percent(float* tank_percent,
		Bouyancy_Direction last_direction,
		uint32_t* last_update);
/* USER CODE END 0 */

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
DMA_HandleTypeDef hdma_tim1_ch1;

/* TIM1 init function */
void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 8;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 35000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */
  HAL_TIM_Base_MspInit(&htim1);

  HAL_TIM_Base_Start(&htim1); //Starts the TIM Base generation
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1); //Starts the PWM signal generation
  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}
/* TIM2 init function */
void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 24000;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 4294967295;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV4;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */
  HAL_TIM_Base_MspInit(&htim2);
  TIM2->CNT = 1;
  /* USER CODE END TIM2_Init 2 */

}
/* TIM3 init function */
void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 30;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 61936;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 4915;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */
  HAL_TIM_Base_MspInit(&htim3);

  HAL_TIM_Base_Start(&htim3); //Starts the TIM Base generation
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1); //Starts the PWM signal generation
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2); //Starts the PWM signal generation
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3); //Starts the PWM signal generation
  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspInit 0 */

  /* USER CODE END TIM1_MspInit 0 */
    /* TIM1 clock enable */
    __HAL_RCC_TIM1_CLK_ENABLE();

    /* TIM1 DMA Init */
    /* TIM1_CH1 Init */
    hdma_tim1_ch1.Instance = DMA1_Stream0;
    hdma_tim1_ch1.Init.Request = DMA_REQUEST_TIM1_CH1;
    hdma_tim1_ch1.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_tim1_ch1.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_tim1_ch1.Init.MemInc = DMA_MINC_ENABLE;
    hdma_tim1_ch1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    hdma_tim1_ch1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
    hdma_tim1_ch1.Init.Mode = DMA_CIRCULAR;
    hdma_tim1_ch1.Init.Priority = DMA_PRIORITY_LOW;
    hdma_tim1_ch1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&hdma_tim1_ch1) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(tim_baseHandle,hdma[TIM_DMA_ID_CC1],hdma_tim1_ch1);

  /* USER CODE BEGIN TIM1_MspInit 1 */

  /* USER CODE END TIM1_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM2)
  {
  /* USER CODE BEGIN TIM2_MspInit 0 */

  /* USER CODE END TIM2_MspInit 0 */
    /* TIM2 clock enable */
    __HAL_RCC_TIM2_CLK_ENABLE();

    /* TIM2 interrupt Init */
    HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM2_IRQn);
  /* USER CODE BEGIN TIM2_MspInit 1 */

  /* USER CODE END TIM2_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM3)
  {
  /* USER CODE BEGIN TIM3_MspInit 0 */

  /* USER CODE END TIM3_MspInit 0 */
    /* TIM3 clock enable */
    __HAL_RCC_TIM3_CLK_ENABLE();
  /* USER CODE BEGIN TIM3_MspInit 1 */

  /* USER CODE END TIM3_MspInit 1 */
  }
}
void HAL_TIM_MspPostInit(TIM_HandleTypeDef* timHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(timHandle->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspPostInit 0 */

  /* USER CODE END TIM1_MspPostInit 0 */
    __HAL_RCC_GPIOE_CLK_ENABLE();
    /**TIM1 GPIO Configuration
    PE9     ------> TIM1_CH1
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /* USER CODE BEGIN TIM1_MspPostInit 1 */

  /* USER CODE END TIM1_MspPostInit 1 */
  }
  else if(timHandle->Instance==TIM3)
  {
  /* USER CODE BEGIN TIM3_MspPostInit 0 */

  /* USER CODE END TIM3_MspPostInit 0 */

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**TIM3 GPIO Configuration
    PA6     ------> TIM3_CH1
    PC7     ------> TIM3_CH2
    PC8     ------> TIM3_CH3
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* USER CODE BEGIN TIM3_MspPostInit 1 */

  /* USER CODE END TIM3_MspPostInit 1 */
  }

}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspDeInit 0 */

  /* USER CODE END TIM1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM1_CLK_DISABLE();

    /* TIM1 DMA DeInit */
    HAL_DMA_DeInit(tim_baseHandle->hdma[TIM_DMA_ID_CC1]);
  /* USER CODE BEGIN TIM1_MspDeInit 1 */

  /* USER CODE END TIM1_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM2)
  {
  /* USER CODE BEGIN TIM2_MspDeInit 0 */

  /* USER CODE END TIM2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM2_CLK_DISABLE();

    /* TIM2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(TIM2_IRQn);
  /* USER CODE BEGIN TIM2_MspDeInit 1 */

  /* USER CODE END TIM2_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM3)
  {
  /* USER CODE BEGIN TIM3_MspDeInit 0 */

  /* USER CODE END TIM3_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM3_CLK_DISABLE();
  /* USER CODE BEGIN TIM3_MspDeInit 1 */

  /* USER CODE END TIM3_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
HAL_StatusTypeDef cmd_motors(struct commands* cmds, struct sensor_data* data_ptr)
{
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, cmds->thruster);
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, cmds->elevator);

	HAL_StatusTypeDef return_val = rudder_control(cmds);

	if (buoyancy_control(cmds, data_ptr) != HAL_OK)
	{
		error_blink();
		return HAL_ERROR;
	}

	return return_val;
}

void init_motors()
{
	pump_up();
	// Sends a 1500 us pulse to each channel
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, ESC_INIT_DUTY);
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, SERVO_0_DUTY);
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, SERVO_0_DUTY);
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, SERVO_0_DUTY);
	HAL_Delay(MIN_PWM_INIT_DELAY_IN_MS); // Needs to be at least this long between the
										 // first pulse and subsequent actions.
	stop_pumps_and_timer();

	// Might need to do more for the pumps.
}

void stop_pumps_and_timer()
{
	 HAL_GPIO_WritePin(Pump_In_GPIO_Port, Pump_In_Pin,  GPIO_PIN_RESET);
	 HAL_GPIO_WritePin(Pump_Out_GPIO_Port, Pump_Out_Pin,  GPIO_PIN_RESET);
	 HAL_TIM_Base_Stop_IT(&htim2);
	 TIM2->CNT = 0;
}

void pump_up()
{
	 HAL_GPIO_WritePin(Pump_In_GPIO_Port, Pump_In_Pin,  GPIO_PIN_RESET);
	 HAL_GPIO_WritePin(Pump_Out_GPIO_Port, Pump_Out_Pin,  GPIO_PIN_SET);
}

static void pump_down()
{
	HAL_GPIO_WritePin(Pump_In_GPIO_Port, Pump_In_Pin,  GPIO_PIN_SET);
 	HAL_GPIO_WritePin(Pump_Out_GPIO_Port, Pump_Out_Pin,  GPIO_PIN_RESET);
}

static HAL_StatusTypeDef rudder_control(struct commands* cmds)
{
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, cmds->rudder);

	if (cmds->rudder_drive == RUDDER_DRIVE_TOGETHER)
	{
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, cmds->rudder);
	}
	else if (cmds->rudder_drive == RUDDER_DRIVE_DIFF)
	{
		uint16_t offset_from_centre = abs(cmds->rudder - SERVO_0_DUTY/2);

		uint16_t diff_rudder = cmds->rudder > SERVO_0_DUTY/2 ?
				cmds->rudder - offset_from_centre : cmds->rudder + offset_from_centre;

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, diff_rudder);
	}
	else
	{
		error_blink();
		return HAL_ERROR;
	}

	return HAL_OK;
}

// TODO: This function needs a review
static HAL_StatusTypeDef buoyancy_control(struct commands* cmds,
		struct sensor_data* data_ptr)
{
	HAL_StatusTypeDef return_val = HAL_OK;

	// Check & update the tank capacity
	update_tank_percent(&data_ptr->tank_percent,
			cmds->last_bouyancy_control,
			&data_ptr->tank_timer);

	// If the pumps stop, stop the timer
	// If the pumps change direction, restart the timer from 0
	// If the tank is full, stop the pumps and the timer

	if (cmds->bouyancy_control == Bouyancy_Direction_HOLD)
	{
		stop_pumps_and_timer();
		cmds->last_bouyancy_control = Bouyancy_Direction_HOLD;
		data_ptr->tank_timer = 0;
	}
	else if (cmds->bouyancy_control == Bouyancy_Direction_DOWN) // Command downwards
	{
		if (data_ptr->tank_percent < MAX_TANK_PERCENT) // If the tank is not full
		{
			if (cmds->last_bouyancy_control != Bouyancy_Direction_DOWN)
			{
				// Restart the timer since we are now going different direction.
				if (restart_buoyancy_timer() == HAL_OK) // If the timer successfully starts
				{
					data_ptr->tank_timer = 0;
					pump_down();
				}
				else // The timer did not start correctly, we can't know how full the tank is
				{
					stop_pumps_and_timer();
					data_ptr->tank_timer = 0;
					error_blink();
					return_val = HAL_ERROR;
				}
			}
		}
		else // The tank is full
		{
			 stop_pumps_and_timer();
		}

		cmds->last_bouyancy_control = Bouyancy_Direction_DOWN;
	}
	else
	{
		// This one is the default because if it's not one of the other two then we
		// either intentionally want to surface or there is an error and we still
		// want to surface.
		 pump_up();

		 if (cmds->last_bouyancy_control != Bouyancy_Direction_UP)
		 {
			// Restart the timer since we are now going different direction.

			 // This isn't a condition for pump_up like it is for down because
			 // Even if the timer fails, we still want to run the pump out to surface
			 return_val = restart_buoyancy_timer();
		 }

		 cmds->last_bouyancy_control = Bouyancy_Direction_UP;

		 if (cmds->bouyancy_control != Bouyancy_Direction_UP)
		 {
			 // If it was an error, return that.
			 return_val = HAL_ERROR;
		 }
	}

	return return_val;
}

static HAL_StatusTypeDef restart_buoyancy_timer()
{
	HAL_TIM_Base_Stop_IT(&htim2);
	return HAL_TIM_Base_Start_IT(&htim2);
}

static void update_tank_percent(float* tank_percent,
		Bouyancy_Direction last_direction,
		uint32_t* last_update)
{
	// Get current time (microseconds)
	uint32_t timer_val = __HAL_TIM_GET_COUNTER(&htim2);
	uint32_t delta_time = timer_val > *last_update ? timer_val - *last_update :
			0;
	*last_update = timer_val;

	float delta_tank_percent = (float)delta_time*TANK_PERCENT_PER_S*TIMER_TO_S;

	*tank_percent += last_direction == Bouyancy_Direction_DOWN ?
			delta_tank_percent : -delta_tank_percent;

	if (*tank_percent >= MAX_TANK_PERCENT)
	{
		stop_pumps_and_timer();
	}
	else if (*tank_percent <= 0)
	{
		// Even if the timer fails, we still want to run the pump out to surface
//		*tank_percent = 0;
	}
}
/* USER CODE END 1 */
